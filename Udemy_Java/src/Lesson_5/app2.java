
package Lesson_5;


public class app2 {

    String color;
    String engine;
    int speed;
    
    // Создадим три метода
    
    int gaz(int skorost){
        speed+=skorost;
        return speed;
    }
    
    
    int tormoz(int skorost){
        speed-=skorost;
        return speed;
    }
    
    void showInfo(){
        System.out.println("Цвет: " + color + " / Двигатель: " + engine + " / Скорость: " + speed + "км/ч");
    }
}

class car {

    public static void main(String[] args) {
        app2 car1 = new app2();
        car1.color  = "White";
        car1.engine = "V8";
        car1.speed  = 60;
        
        // Вызываем наши методы
        // Показываем информацию о машине
        car1.showInfo();
        
        // Прибавляем скорость и показываем информацию о машине
        car1.gaz(20);
        car1.showInfo();
        
        // Уменьшаем скорость и показываем информацию о машине
        car1.tormoz(40);
        car1.showInfo();
    }



}