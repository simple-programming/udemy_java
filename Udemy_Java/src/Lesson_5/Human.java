package Lesson_5;

public class Human {
    
    String name;
    Car car;
    BankAccount bA;
    
    // Метод info, который не имеет ретурна, который только возвращает на экран данные о человеке
    void info(){
        System.out.println("Имя: " + name + " / " + "Цвет авто: " + car.color + " / " +  "Баланс: " +  bA.balance + "$");
    }
}


class HumanInfo {
    public static void main(String[] args){
        Human man = new Human();
        man.name = "Alex";
        man.car = new Car("Red", "V4");
        man.bA = new BankAccount(1, 200.5);
        
        // Обратимся к методу info
        man.info();
   
    }
}

class Car{
    // Define Конструкто Car
    Car(String c, String e){
        color  = c;
        engine = e;
    }
    
    String color;
    String engine;
}


class BankAccount{
    // Define Конструкто BankAccount
    BankAccount(int i, double b){
        id = i;
        balance = b;
    }
    
    int id;
    double balance;
}