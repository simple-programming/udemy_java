
package Lesson_5;

public class app3 {
    
    //
    // Создадим метод summa, который принимает три значения и суммирует их
    //
    int summa(int a, int b, int c){
        
        int result = a + b + c;
        return result;
        
    }
    
    //
    // Создадим еще один метод для подсчета среднеарифметического значаения и в нем используем метод summa
    //
    int sredneeArifmeticheskoe(int a1, int b1, int c1){
   
        int sa_result = summa(a1, b1, c1)/3;
        return sa_result;
   
    }
    
}

//
//
//
class sum {

    public static void main(String[] args) {
        
        app3 t = new app3(); // тип данных app3 переменной t присвоить значение нового объекта класса app3
        int SummaTrexChisel = t.summa(3, 3, 3); // вызов метода summa
        System.out.println("SummaTrexChisel = " + SummaTrexChisel);
        
        System.out.println();
        
        int sredneeArifmeticheskoeTrexChisel = t.sredneeArifmeticheskoe(10, 20, 30);
        System.out.println("sredneeArifmeticheskoe = " + sredneeArifmeticheskoeTrexChisel);
        
        System.out.println();
        
        System.out.println(new app3().sredneeArifmeticheskoe(60, 100, 20)); // Одноразовый объект, результат метода которго мы просто вывели на экран
        System.out.println(new app3().summa(1, 2, 3)); // Одноразовый объект, результат метода которго мы просто вывели на экран
    }

}