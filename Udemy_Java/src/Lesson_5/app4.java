package Lesson_5;

public class app4 {
        
    // Default constructor генериться автоматом как бы в фоне
    // app4(){} -- вот так выглядит
    // Если мы его задали вручную, то это уже Define constructor, даже если он пустой!
    
    //
    // Создадим констрктор с заданными значениями
    //
    app4(String cvet, String motor){
        color = cvet;
        engine = motor;
    }
    
    
    String color;
    String engine;

}

class Car{

    public static void main(String[] args){
        
        // Создадим новый объект car1
        // Для его создания нам необходимо ОБЯЗАТЕЛЬНО указать cvet и motor -- new app4("White", "V8");
        app4 car1 = new app4("White", "V8");
        
        System.out.println(car1.color);
        System.out.println(car1.engine);

    }


}
