
package Lesson_2;


public class app {
 
    public static void main(String[] args) {
    byte b1 = 127; /* {-128:127} */
    byte b2 = -128;

/*  byte b3 = 128; -- mistake MAX byte {-128:127}*/

    short sh1 = 32767; /* {-32768:32767} */
    short sh2 = -32768; 

    int i1 = 2147483647; /* {-2147483648:2147483647} */
    int i2 = -2147483648; /* тип int по умолчанию */

    long l1 = 9223372036854775807L; /* {9223372036854775807:-9223372036854775808} */
    long l2 = -9223372036854775808L; /* L - на конце значения для валидации типа long */

    float f1 = 3.14F; /* F - на конце значения для валидации типа float */
    float f2 = 2.5F;

    double d1 = 5.5; /* тип double по умолчанию */
    double d2 = 87.65;

    char c1 = 'A'; /* только один символ для значения + берется в одинарные кавычки */
    char c2 = 'ф';
    char c3 = ' '; /* пробел это тоже один символ */
    char c4 = 1280; /* 1280-й символ в таблице юникода в 10-теричной системе исчеслений */
    char c5 = '\u0500'; // всегда 4 символа, 500-й символ в таблица юникода в 16-теричной системе исчеслений

    boolean bool1 = true; // только true or false
    boolean bool2 = false;

    int a1 = 60; // число в 10-теричной системе исчеслений 
    int a2 = 0B111100; //число 60 в 2-чной системе исчеслений, 0B - указатель на 2-ю систему
    int a3 = 074; //число 60 в 8-ричной системе исчеслений, 0 - указатель на 8-ю систему
    int a4 = 0x3c; //число 60 в 16-ричной системе исчеслений, 0x - указатель на 16-ю систему

    int u1 = 1_000_00____0; // для удобаства записи, можно делить число underscore-ом, вывод будет целым 1000000


    // ######################################## // 
    System.out.println(b1);
    System.out.println(b2);
    System.out.println(sh1);
    System.out.println(sh2);
    System.out.println(i1);
    System.out.println(i2);
    System.out.println(l1);
    System.out.println(l2);
    System.out.println(f1);
    System.out.println(f2);
    System.out.println(d1);
    System.out.println(d2);
    System.out.println(c1);
    System.out.println(c2);
    System.out.println(c3);
    System.out.println(c4);
    System.out.println(c5);
    System.out.println(bool1);
    System.out.println(bool2);
    System.out.println(a1);
    System.out.println(a2);
    System.out.println(a3);
    System.out.println(a4);
    System.out.println(u1);
    /* ######################################## */ 
    }

}
