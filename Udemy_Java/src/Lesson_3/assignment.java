package Lesson_3;

public class assignment {

    public static void main(String[] args) {
        int a = 5; // = это оператор присваивания, он работает справа налево
        a += 3; // тоже самое, что "a = a + 3;"
        System.out.println(a);
        System.out.println();

        a /= 2; // тоже самое, что разделить новое значение а из предыдущего равентсва "a = a / 2;"
        System.out.println(a);
        System.out.println();

        a *= 2; // тоже самое, "a = a * 2;"
        System.out.println(a);
        System.out.println();

        a -= 4; // тоже самое, "a = a - 4;"
        System.out.println(a);
        System.out.println();
    }
    
}
