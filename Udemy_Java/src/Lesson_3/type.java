package Lesson_3;


public class type {
    
    public static void main(String[] args) {
    
        int a=3, b=5, c=10; // Можно переменные одного типа записывать в одну строку
        int e=6;

        int f=e;
        int ff=2*e; // Можно присваивать значения одной переменной другой и сразу производить вычисление
        System.out.println(ff+f); // Можно вычисление делать сразу на экран
        System.out.println(ff);
        System.out.println(b);


        System.out.println(); // Пустая строка

        int d = c/a; // Т.к. d это INT то дробная часть просто отсекаестя (не округляется!)
        System.out.println(d);
        System.out.println(c/a); // INT:INT=INT дробная часть тоже отсечется

        System.out.println();

        double x = 10;
        double y = 3;
        System.out.println(x/y); // DOUBLE:DOUBLE=DOUBLE дробная часть остается

        System.out.println(); 

        int aa = 10;
        int bb = 3;
        double cc = 5.5;
        double dd = 3.5;
        System.out.println(aa%bb); // Остаток от деления (3 вместилось в "10" 3 раза и остался 1)
        System.out.println(cc%dd);

        /*
         ++ -- 
         это прибавить один, и отнять один (увеличение на единицу / уменьшение на единицу)
            for (i=1; i < 10; i++) 
            for (i=10; i > 1; i--) 
        */

        int pr1 = 5, pr2 = 3; // Для примера c префиксом 
        int po1 = 5, po2 = 3; // Для примера c постфиксом
        int prefix = pr1 - ++pr2; // префикс сразу прибавляет 1, до вычитания "5 - (1+3)" Prefix=1 
        int postfix = po1 - po2++; // постфикс прибавляет 1 после вычитания "5 - 3; 3+1" Postfix=2 
        System.out.println("Prefix = " + prefix);
        System.out.println("Postfix = " + postfix);
        System.out.println("pr2 = " + pr2 );
        System.out.println("po2 = " + po2 );
    }

}
