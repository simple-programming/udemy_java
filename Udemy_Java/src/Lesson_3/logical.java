package Lesson_3;

public class logical {
    
    public static void main(String[] args) {
    
        boolean a = true;
        boolean b = false;
        boolean c = true;
        
        boolean and_result = a && b && c; // && (and) -- все условия должны совпасть
        boolean or_result = a || b || c; // || (or) -- хотябы одно условие должно совпасть

        System.out.println("and_result = " + and_result);
        System.out.println("or_result = " + or_result);
        System.out.println();

        System.out.println(b = !a); // a=true, значит b=false, т.к. b не равно a (остается только один вариант)
        System.out.println();

        System.out.println(!a); // не true - значит false
        System.out.println(!b); // не false - значит true
        System.out.println();

        int x = 5;
        int y = 10;
        boolean z = x<y;
        boolean v = !(x<y);
        System.out.println(!z); // x<y=true, но не (!)true=false, получаем false
        System.out.println(v); // тоже самое что и предыдущий пример
        System.out.println();

        int k = 50;
        int l = 100;
        int m = 0;
        boolean e = k > l && l == ++m && m != l; // "Short circle - AND" - если есть один false дальше строка не обрабатывается 
        boolean i = k > l || l == m || l > m; // "Short circle - OR" - если есть один true дальше строка не обрабатывается 
        System.out.println("e = " + e);
        System.out.println("i = " + i);
        System.out.println("m = " + m); // "Short circle" дальше e = k > l компилятор не пойдет, т.к. условие AND не выполненно
        System.out.println();

        int w = 50;
        int r = 100;
        int g = 0;
        boolean h = w > r & r == ++g & g != r; // Т.к. используется одинарный AND вся строка читается до конца
        boolean j = r > w | r == w | g > w; // Т.к. используется одинарный OR вся строка читается до конца
        System.out.println("h = " + h);
        System.out.println("j = " + j);
        System.out.println("g = " + g); // компилятор присвоит 1 для g, т.к. выпонит всю строку "boolean h = w > r & r == ++g & g != r;"
        System.out.println();

        /* 
            ^ -- исключающий или "exclusive bitwise or"
            0 = false
            1 = true
            011 = false
            001 = true
        */
        boolean b1 = true;
        boolean b2 = true;
        boolean b3 = true;
        boolean b4 = false;
        boolean b5 = false;
        System.out.print("111 = ");
        System.out.println(b1^b2^b3);
        System.out.print("001 = ");
        System.out.println(b4^b5^b3);
        System.out.print("101 = ");
        System.out.println(b1^b4^b3);
        System.out.print("011 = ");
        System.out.println(b4^b1^b2);
        System.out.print("1011 = ");
        System.out.println(b1^b4^b2^b3);
        System.out.println();

        /* 

        */
        char unicode = 'a'; // Символ 'a' = 97 порядковому номеру в юникоде
        int integer = 10;
        System.out.println(unicode + integer); // 97 + 10 = 107
        System.out.println();

    }
    
}
