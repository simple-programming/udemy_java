package Lesson_4;

public class BankAccount {
 
    int id;
    String name;
    // String name2 = new String("Peter"); // string можно задавать 2 способами
    double balance;
    
}


class Lesson4_BankAccountTest{
    public static void main(String[] args) {
        BankAccount User1 = new BankAccount();
        BankAccount User2 = new BankAccount();
        BankAccount User3 = new BankAccount();

        User1.id = 1;
        User1.name = "Alexander";
        User1.balance = 10000.38;

        User2.id = 2;
        User2.name = "Oksana";
        User2.balance = 199990.38;

        User3.id = 3;
        User3.name = "Arina";
        User3.balance = 98989898.38;

        System.out.println(User1.name + " "+ User2.name + " "+ User3.name);
        System.out.println();
        System.out.println(new BankAccount().id); // Одноразовый объект. Мы его создали и забыли о нём, т.к. на него нет ссылки. Мы не присвоили ни одной переменной этот объект.

    }
}