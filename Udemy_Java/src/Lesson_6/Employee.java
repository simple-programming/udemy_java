package Lesson_6;


public class Employee {
    //
    // Создадим три разных коструктора (Overload), для разных ситуаций при трудоустройстве
    // 1. Принимаем кандидата с полным списком данных -- трудостуройство
    // 2. Кандидат после нескольких интервью
    // 3. Кандидат с которым еще не говорили
    //
    Employee(int i, String n, String s, int a, double sal, String dep){
        id = i;
        name = n;
        surname = s;
        age = a;
        salary = sal;
        department = dep;
    
    }
    
    Employee(String n, String s, int a, String dep){
        name = n;
        surname = s;
        age = a;
        department = dep;
    
    }
    
    Employee(String n, String s){
        name = n;
        surname = s;
    }
    
    
    int id;
    String name;
    String surname;
    int age;
    double salary;
    String department;
    
}

class EmployeeTest {

    public static void main(String[] args){
    
        Employee emp1 = new Employee(13, "Ivan", "Ivanov", 25, 1300.0, "IT");
        System.out.println(emp1.name);
        
        System.out.println();
        
        Employee emp2 = new Employee("Alex", "Sidorov", 25, "IT");
        System.out.println(emp2.name);
        
        System.out.println();
        
        Employee emp3 = new Employee("Oleg", "Zaycev");
        System.out.println(emp3.name);
        
    
    }

}