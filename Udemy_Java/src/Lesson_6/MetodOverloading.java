package Lesson_6;

public class MetodOverloading {
    
    //
    // У всех методов одинаковое название, однако типы данных втнутри методов отличаются
    // Это даёт нам возможность воспользоваться перегрузкой
    // Джава сама поймет, в зависимости от типа данных внутри, какой метод использовать
    //
    void show(int i){
        System.out.println(i);
    }
    
    void show(boolean b){
        System.out.println(b);
    }
   
    void show(String s){
        System.out.println(s);
    }
    
}

class Test{

    public static void main(String[] args){
        
        MetodOverloading mo = new MetodOverloading();
        int a = 500;
        mo.show(a);
        
        boolean b = true;
        mo.show(b);
        
        String s = "Alex";
        mo.show(s);
    
    }
}
