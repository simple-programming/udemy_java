package Lesson_6;

public class This {
    //
    // Создадим три разных коструктора (Overload), для разных ситуаций при трудоустройстве
    // 1. Принимаем кандидата с полным списком данных -- трудостуройство
    // 2. Кандидат после нескольких интервью
    // 3. Кандидат с которым еще не говорили
    //
    This(int id1, String name1, String surname1, int age1, double salary1, String department1){
        this(name1, surname1, age1);
        id = id1;
        salary = salary1;
        department = department1;
    
    }
    
    This(String name2, String surname2, int age2, double salary2, String department2){
        this(name2, surname2, age2);
        salary = salary2;
        department = department2;
    
    }
    
    This(String name3, String surname3, int age3){
        name = name3;
        surname = surname3;
        age = age3;
    }
    
    
    int id;
    String name;
    String surname;
    int age;
    double salary;
    String department;
        
    
}


class EmployeeTest {

    public static void main(String[] args){
    
        This emp1 = new This(13, "Ivan", "Ivanov", 25, 1300.0, "IT");
        System.out.println(emp1.name);
        
        System.out.println();
        
        This emp2 = new This("Alex", "Sidorov", 25, 700.0, "IT");
        System.out.println(emp2.name);
        
        System.out.println();
        
        This emp3 = new This("Oleg", "Zaycev", 25);
        System.out.println(emp3.name);
        
    
    }

}